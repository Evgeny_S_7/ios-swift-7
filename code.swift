// п.2
import Foundation

func getAddress(off value: AnyObject) -> String {
    return "\(Unmanaged.passUnretained(value).toOpaque())"
}

class IOSCollectionContainer<T> {
    var items: [T]
    
    init(items: [T]) {
        self.items = items
    }
    
    func append(_ item: T) {
        items.append(item)
    }
    
    func copy() -> IOSCollectionContainer<T> {
        return IOSCollectionContainer<T>(items: self.items)
    }
}

struct IOSCollection<T> {
    var container: IOSCollectionContainer<T>
    
    init(container: IOSCollectionContainer<T>) {
        self.container = container
    }
    
    mutating func append(_ item: T) {
        if !isKnownUniquelyReferenced(&container) {
            container = container.copy()
        }
        container.append(item)
    }
}

var array: [Int] = [2, 45, 192, 3, 0, 24, 333, 56, 1, 8]

var container = IOSCollectionContainer(items: array)
var collection = IOSCollection(container: container) 
var collection2 = collection

print(getAddress(off: collection.container))
print(getAddress(off: collection2.container))

collection2.append(100500)
print()

print(getAddress(off: collection.container))
print(getAddress(off: collection2.container))
print()


// п.3
protocol Hotel {
    var roomCount: Int { get }
    
    init(roomCount: Int)
}

class HotelAlfa: Hotel {
    var roomCount: Int
    
    required init(roomCount: Int) {
        self.roomCount = roomCount
    }
}


// п.4-5
protocol GameDice {
    var numberDice: Int { get }
}

extension Int: GameDice {
    var numberDice: Int {
        print("Выпало \(self) на кубике")
        return self
    }
}

let diceCoub = 4
let whatIs = diceCoub.numberDice
print()

/*@objc protocol ProtocolExample {
    func funcExample()
    
    var value: Int { get }
    
    @objc optional var optionalValue: String? { get }
}

class ClassExample: ProtocolExample {
    func funcExample() {
        print("Hello!")
    }
    
    var value: Int
    
    init(value: Int) {
        self.value = value
    }
}
let classExample = ClassExample(value: 5)
classExample.funcExample()*/


// п.6-7 - теоретические


// п.8-10
protocol Coding {
    var time: Int { get set }
    var codeAmount: Int { get set }
    
    func writeCode(platform: Platform, numberOfSpecialist: Int)
}

protocol StopCoding {
    func stopCoding()
}

enum Platform {
    case IOS, Android, Web
}

class Company: Coding, StopCoding {
    var time: Int
    var codeAmount: Int
    var programmersAmount: Int
    var platforms: [Platform]
    
    init(programmersAmount: Int, platforms: [Platform]) {
        self.programmersAmount = programmersAmount
        self.platforms = platforms
        self.time = 0
        self.codeAmount = 0
    }
    
    func writeCode(platform: Platform, numberOfSpecialist: Int) {
        guard platforms.contains(platform) else {
            print("Мы не работаем с \(platform)")
            return
        }
        
        guard numberOfSpecialist <= programmersAmount else {
            print("Недостаточно программистов")
            return
        }
        
        switch platform {
            case .IOS:
                time += 10 * numberOfSpecialist
                codeAmount += 100 * numberOfSpecialist
            case .Android:
                time += 15 * numberOfSpecialist
                codeAmount += 80 * numberOfSpecialist
            case .Web:
                time += 5 * numberOfSpecialist
                codeAmount += 120 * numberOfSpecialist
        }
        
        print("Разработка началась. пишем код \(platform)")
    }
    
    func stopCoding() {
        print("Работа закончена. Сдаю в тестирование")
    }
}

var platforms = [Platform]()

platforms.append(Platform.IOS)

let company = Company(programmersAmount: 15, platforms: platforms)

company.writeCode(platform: .IOS, numberOfSpecialist: 5)
company.stopCoding()
print()

company.writeCode(platform: .IOS, numberOfSpecialist: 20)
company.stopCoding()
print()

company.writeCode(platform: .Web, numberOfSpecialist: 7)
company.stopCoding()
print()